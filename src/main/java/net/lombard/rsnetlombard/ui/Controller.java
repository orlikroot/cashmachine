/*
 * Copyright (c) 2017. Eugene Krotov. e.krotov@hotmail.com
 */

package net.lombard.rsnetlombard.ui;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import lombok.extern.slf4j.Slf4j;
import net.lombard.rsnetlombard.domain.Status;
import net.lombard.rsnetlombard.services.CashMachineService;
import net.lombard.rsnetlombard.services.CashMachineServiceImpl;
import net.lombard.rsnetlombard.services.Connection;
import net.lombard.rsnetlombard.services.ConnectionImpl;
import net.lombard.rsnetlombard.utils.ResultData;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * @author Eugene Krotov e.krotov@hotmail.com
 * @version 1.0
 */
@Slf4j
public class Controller implements Initializable {


    private static final String MANUAL_CASH_IN = "СЛУЖБОВЕ ВНЕСЕННЯ (Р)";
    private static final String MANUAL_CASH_OUT = "СЛУЖБОВА ВИДАЧА (Р)";
    private static final String UNREGISTERED = "Не зарегестрирован";
    private static final String LOCKED = "Заблокирован";
    private static final String UNLOCKED = "Рабочий режим";

    private static final String OPENED = "Открыта";
    private static final String CLOSED = "Закрыта";

    private static final String CONNECTION_OK = "Соединение установлено";
    private static final String CONNECTION_LOST = "Обрыв соединения";

    private static final String ERROR = "Ошибка";

    private static final String ABOUT = "Ломбрард ОНИКС © 2017\n" +
            "Фискальный регистратор МИНИ ФП-54.01\n";

    private CashMachineService machineService;
    private Connection connection;

    @FXML
    private Label cashMachineIdLabel;
    @FXML
    private Label cashierIdLabel;
    @FXML
    private Label dateShiftLabel;
    @FXML
    private Label timeShiftLabel;
    @FXML
    private Label isBlockLabel;
    @FXML
    private Label shiftStatusLabel;
    @FXML
    private Label errorLabel;

    @FXML
    private TextField loginInput;
    @FXML
    private PasswordField passwordInput;

    @FXML
    private Label cashBoxLabel;
    @FXML
    private TextField cashInInput;
    @FXML
    private TextField cashOutInput;

    @FXML
    private TextField docIdAddInput;
    @FXML
    private TextField docIdReturnInput;
    @FXML
    private TextField addBailLoanInput;
    @FXML
    private TextField returnBailLoanInput;
    @FXML
    private TextField returnBailPercentInput;
    @FXML
    private TextField returnBailFineInput;
    @FXML
    private TextField returnBailBonusInput;

    @FXML
    private ProgressBar progressBar;

    @FXML
    private Label aboutLabel;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        machineService = new CashMachineServiceImpl();
        connection = new ConnectionImpl();
        aboutLabel.setText(ABOUT);
    }

    @FXML
    private void handleInfoButtonAction(ActionEvent e) {
        Status status = machineService.getStatus();
        cashMachineIdLabel.setText(machineService.getCashMachineSN());
        cashierIdLabel.setText(status.getCashierId() == 0 ? UNREGISTERED : String.valueOf(status.getCashierId()));
        dateShiftLabel.setText(status.getDateStartCurrentShift());
        timeShiftLabel.setText(status.getTimeStartCurrentShift());
        isBlockLabel.setText(status.isCashMachineBlockByReport() || status.isCashMachineBlockByPersonalization() ? LOCKED : UNLOCKED);
        shiftStatusLabel.setText(status.isShiftOpen() ? OPENED : CLOSED);

        errorLabel.setText(machineService.getLastError());
    }

    @FXML
    private void handleCashierRegistrationButtonAction(ActionEvent e) {
        String result = machineService.cashierRegistration(loginInput.getText(), passwordInput.getText()).getDescription();

        errorLabel.setText(result);
    }

    @FXML
    private void handleReportXButtonAction(ActionEvent e) {
        ResultData resultData = machineService.printXReport();

        errorLabel.setText(resultData.getDescription());
    }

    @FXML
    private void handleReportZButtonAction(ActionEvent e) {
        ResultData resultData = machineService.printZReport();

        errorLabel.setText(resultData.getDescription());
    }

    @FXML
    private void handleCashBoxButtonAction(ActionEvent e) {
        String sum = String.valueOf(machineService.getCashBoxSum());
        cashBoxLabel.setText(sum + ".00 грн");

        errorLabel.setText(machineService.getLastError());
    }

    @FXML
    private void handleCashInButtonAction(ActionEvent e) {
        String sumStr = cashInInput.getText();
        Integer sum = Integer.parseInt(sumStr);

        ResultData resultData = machineService.cashInOut(sum, CashMachineServiceImpl.IN, MANUAL_CASH_IN);

        errorLabel.setText(resultData.getDescription());

        cashInInput.clear();
    }

    @FXML
    private void handleCashOutButtonAction(ActionEvent e) {
        String sumStr = cashOutInput.getText();
        Integer sum = Integer.parseInt(sumStr);

        ResultData resultData = machineService.cashInOut(sum, CashMachineServiceImpl.OUT, MANUAL_CASH_OUT);

        errorLabel.setText(resultData.getDescription());

        cashOutInput.clear();
    }

    @FXML
    private void handleServiceReconnectButtonAction(ActionEvent e) {
        new ProgressReconnect(progressBar);
        connection = connection.reconnect();
    }

    @FXML
    private void handleCancelReceiptButtonAction(ActionEvent e) {
        machineService.cancelReceipt();

        errorLabel.setText(machineService.getLastError());
    }

    @FXML
    private void handleAddBailButtonAction(ActionEvent e) {
        String docId = docIdAddInput.getText();
        String sumStr = addBailLoanInput.getText();

        if (docId.isEmpty() || sumStr.isEmpty()) {
            errorLabel.setText(ERROR);
            createAlert("Предупреждение", "Ошибка", "Заполните поля 'Договор', 'Кредит'", Alert.AlertType.WARNING);
        } else {
            ResultData resultData = machineService.addBail(docId, Integer.parseInt(sumStr));
            errorLabel.setText(resultData.getDescription());
        }

        docIdAddInput.clear();
        addBailLoanInput.clear();
    }

    @FXML
    private void handleReturnBailButtonAction(ActionEvent e) {

        String docId = docIdReturnInput.getText();
        String sumStr = returnBailLoanInput.getText();
        String percentStr = returnBailPercentInput.getText();
        String fineStr = returnBailFineInput.getText();
        String bonusStr = returnBailBonusInput.getText();

        if (docId.isEmpty() || sumStr.isEmpty() || percentStr.isEmpty() || fineStr.isEmpty() || bonusStr.isEmpty()) {
            errorLabel.setText(ERROR);
            createAlert("Предупреждение", "Ошибка", "Заполните поля 'Договор', 'Кредит', \n'Процент','Пеня','Бонус'", Alert.AlertType.WARNING);
        } else {
            ResultData resultData = machineService.returnBail(docId, Integer.parseInt(sumStr), Integer.parseInt(percentStr), Integer.parseInt(fineStr), Integer.parseInt(bonusStr));
            errorLabel.setText(resultData.getDescription());
        }

        docIdReturnInput.clear();
        returnBailLoanInput.clear();
        returnBailPercentInput.clear();
        returnBailFineInput.clear();
        returnBailBonusInput.clear();
    }

    @FXML
    private void handlePrintEmptyReceiptButtonAction(ActionEvent e) {
        ResultData resultData = machineService.printEmptyReceipt();
        errorLabel.setText(resultData.getDescription());
    }

    private void createAlert(String title, String header, String content, Alert.AlertType type) {
        Alert alert = new Alert(type);
        alert.setTitle(title);
        alert.setHeaderText(header);
        alert.setContentText(content);

        alert.showAndWait();
    }

    private class ProgressReconnect implements Runnable {

        private ProgressBar progressBar;

        public ProgressReconnect(ProgressBar progressBar) {
            this.progressBar = progressBar;
            Thread thread = new Thread(this);
            thread.start();
        }

        @Override
        public void run() {
            try {
                progressBar.setStyle("-fx-accent: green; ");
                progressBar.setVisible(true);
                progressBar.setProgress(0.01);
                boolean tryConnect = true;
                double progress = 0.05;
                while (tryConnect) {
                    progressBar.setProgress(progress);
                    progress = progress + 0.05;
                    if (connection.getCashMachineSN() == null) {
                        if (progress > 0.95) {
                            progressBar.setStyle("-fx-accent: red; ");
                            progressBar.setProgress(1.0);
                            tryConnect = false;
                        }
                        Thread.sleep(1000);
                    } else {
                        progressBar.setProgress(1.0);
                        tryConnect = false;
                    }
                    if (!tryConnect) {
                        Thread.sleep(3000);
                        progressBar.setVisible(false);
                        Thread.currentThread().interrupt();
                    }
                }
            } catch (InterruptedException e1) {
                e1.printStackTrace();
            }
        }
    }
}
