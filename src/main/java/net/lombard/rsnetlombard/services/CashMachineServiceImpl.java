/*
 * Copyright (c) 2017. Eugene Krotov. e.krotov@hotmail.com
 */

package net.lombard.rsnetlombard.services;

import com.jacob.activeX.ActiveXComponent;
import com.jacob.com.Dispatch;
import lombok.extern.slf4j.Slf4j;
import net.lombard.rsnetlombard.domain.Status;
import net.lombard.rsnetlombard.exeptions.ReceiptException;
import net.lombard.rsnetlombard.utils.CommandsUtils;
import net.lombard.rsnetlombard.utils.ResultData;
import net.lombard.rsnetlombard.utils.StatusUtils;


/**
 * @author Eugene Krotov e.krotov@hotmail.com
 * @version 1.0
 */
@Slf4j
public class CashMachineServiceImpl implements CashMachineService {

    public static final int RECEIPT_SELL = 0;
    public static final int RECEIPT_RETURN = 1;

    public static final int CASH = 0;

    public static final int IN = 0;
    public static final int OUT = 1;

    public static final int SURCHARGE_FLAG = 0;
    public static final int DISCOUNT_FLAG = 1;

    public static final int BAIL_PLU_CODE = 1;
    public static final int PERCENT_PLU_CODE = 2;
    public static final int FINE_PLU_CODE = 3;

    /**
     * Операция внос/выдача денег
     * @param sum
     * @param inOutFlag
     * @param comment
     * @return 0 if complete without ERROR
     */
    @Override
    public ResultData cashInOut(int sum, int inOutFlag, String comment) {
        log.info("CASH IN_OUT OPERATION START. | sum = {} | type: {} |", sum, inOutFlag == IN ? "IN" : "OUT");
        ResultData resultData = ResultData.UNKNOWN;
        Dispatch dispatch = loadLib(Connection.PROGRAM_ID);
        try {
            resultData = openPort(dispatch, Connection.CASH_MACHINE_COM_PORT);

            resultData = inOutOperation(dispatch, inOutFlag, sum, comment);

            log.info("CASH IN_OUT OPERATION END.");
            log.info("------------------------------------------------------------");
            return resultData;
        } catch (Exception e) {
            log.error("*** InOut operation FAIL. Error: [" + e.getMessage() + "]");
            log.error("------------------------------------------------------------");
        } finally {
            closePort(dispatch); // закрываем порт
        }
        return resultData;
    }

    /**
     * Выдача залога
     * @param bailIdent
     * @param sum
     * @return ResultData
     */
    @Override
    public ResultData addBail(String bailIdent, int sum) {
        log.info("ADD BAIL OPERATION START. | bailIdent: {} | sum = {}", bailIdent, sum);
        ResultData resultData = ResultData.UNKNOWN;
        Dispatch dispatch = loadLib(Connection.PROGRAM_ID);
        try {
            resultData = openPort(dispatch, Connection.CASH_MACHINE_COM_PORT);

            resultData = openReceipt(dispatch, RECEIPT_RETURN); // Открываем чек

            resultData = salePlu(dispatch, sum, BAIL_PLU_CODE);

            resultData = addComment(dispatch, bailIdent);

            resultData = pay(dispatch, sum);

            lastReceiptCopy(dispatch);

            log.info("ADD BAIL OPERATION END.");
            log.info("------------------------------------------------------------");
        } catch (Exception e) {
            log.error("*** AddBail operation FAIL. Error: [" + e.getMessage() + "]");
            log.error("------------------------------------------------------------");
        } finally {
            closePort(dispatch); // закрываем порт
        }
        return resultData;
    }

    /**
     * Продажа залога из ломбарда
     * @param bailIdent
     * @param sum
     * @return
     */
    @Override
    public ResultData sellBail(String bailIdent, int sum) {
        log.info("SELL BAIL OPERATION START. | bailIdent: {} | sum = {}", bailIdent, sum);
        ResultData resultData = ResultData.UNKNOWN;
        Dispatch dispatch = loadLib(Connection.PROGRAM_ID);
        try {
            resultData = openPort(dispatch, Connection.CASH_MACHINE_COM_PORT);

            resultData = openReceipt(dispatch, RECEIPT_SELL); // Открываем чек

            resultData = salePlu(dispatch, sum, BAIL_PLU_CODE);

            resultData = addComment(dispatch, bailIdent);

            resultData = pay(dispatch, sum);

            log.info("SELL BAIL OPERATION END.");
            log.info("------------------------------------------------------------");
        } catch (Exception e) {

            log.error("*** SellBail operation FAIL. Error: [" + e.getMessage() + "]");
            log.info("------------------------------------------------------------");
        } finally {
            closePort(dispatch); // закрываем порт
        }
        return resultData;
    }

    /**
     * Перенос залога в магазин
     * @param bailIdent
     * @param sum
     * @return
     */
    @Override
    public ResultData toShopBail(String bailIdent, Integer sum) {
        log.info("TO SHOP BAIL OPERATION START. | bailIdent: {} | sum = {}", bailIdent, sum);
        ResultData resultData = ResultData.UNKNOWN;
        Dispatch dispatch = loadLib(Connection.PROGRAM_ID);
        try {
            resultData = openPort(dispatch, Connection.CASH_MACHINE_COM_PORT);

            resultData = openReceipt(dispatch, RECEIPT_SELL); // Открываем чек

            resultData = salePlu(dispatch, sum, BAIL_PLU_CODE);

            resultData = addComment(dispatch, bailIdent);

            resultData = pay(dispatch, sum);

            log.info("TO SHOP BAIL OPERATION END.");
            log.info("------------------------------------------------------------");
        } catch (Exception e) {

            log.error("*** ToShopBail operation FAIL. Error: [" + e.getMessage() + "]");
            log.info("------------------------------------------------------------");
        } finally {
            closePort(dispatch); // закрываем порт
        }
        return resultData;
    }

    /**
     * Удалить залог
     * @param bailIdent
     * @param sum
     * @return
     */
    @Override
    public ResultData deleteBail(String bailIdent, Integer sum) {
        log.info("DELETE BAIL OPERATION START. | bailIdent: {} | sum = {}", bailIdent, sum);
        ResultData resultData = ResultData.UNKNOWN;
        Dispatch dispatch = loadLib(Connection.PROGRAM_ID);
        try {
            resultData = openPort(dispatch, Connection.CASH_MACHINE_COM_PORT);

            resultData = openReceipt(dispatch, RECEIPT_SELL); // Открываем чек

            resultData = salePlu(dispatch, sum, BAIL_PLU_CODE);

            resultData = addComment(dispatch, bailIdent);

            resultData = pay(dispatch, sum);

            log.info("DELETE BAIL OPERATION END.");
            log.info("------------------------------------------------------------");
        } catch (Exception e) {

            log.error("*** DeleteBail operation FAIL. Error: [" + e.getMessage() + "]");
            log.info("------------------------------------------------------------");
        } finally {
            closePort(dispatch); // закрываем порт
        }
        return resultData;
    }

    /**
     * Возврат залога
     * @param bailIdent
     * @param sum
     * @param percent
     * @param fine
     * @param bonus
     * @return ResultData
     */
    @Override
    public ResultData returnBail(String bailIdent, int sum, int percent, int fine, int bonus) {
        log.info("RETURN BAIL OPERATION START. | bailIdent: {} | sum = {} | percent: {} | fine: {} | bonus: {}", bailIdent, sum, percent, fine, bonus);
        ResultData resultData = ResultData.UNKNOWN;
        Dispatch dispatch = loadLib(Connection.PROGRAM_ID);
        try {
            resultData = openPort(dispatch, Connection.CASH_MACHINE_COM_PORT);

            resultData = openReceipt(dispatch, RECEIPT_SELL); // Открываем чек

            resultData = salePlu(dispatch, sum, BAIL_PLU_CODE);

            resultData = addComment(dispatch, bailIdent);

            if (percent > 0) {
                resultData = salePlu(dispatch, percent, PERCENT_PLU_CODE);
            }
            if (fine > 0) {
                resultData = salePlu(dispatch, fine, FINE_PLU_CODE);
            }

            resultData = addDiscountSurcharge(dispatch, DISCOUNT_FLAG, bonus);

            Dispatch.call(dispatch, Connection.CASHMACHINE_NAME, CommandsUtils.SHOW_SUBTOTAL + ";");
            log.info("Total to pay: [{}], calculated: [{}]", sum + percent + fine - bonus, getLastResultInfo(dispatch));

            resultData = pay(dispatch, sum + percent + fine - bonus);

            lastReceiptCopy(dispatch);

            log.info("RETURN BAIL OPERATION END.");
            log.info("------------------------------------------------------------");
        } catch (Exception e) {
            log.error("*** ReturnBail operation FAIL. Error: [" + e.getMessage() + "]");
            log.error("------------------------------------------------------------");
        } finally {
            closePort(dispatch); // закрываем порт
        }
        return resultData;
    }

    /**
     *
     * @return -1 if complete with ERROR
     */
    @Override
    public int getCashBoxSum() {
        log.info("GET CASHBOX OPERATION START.");
        Dispatch dispatch = loadLib(Connection.PROGRAM_ID);
        Integer result = ResultData.UNKNOWN.getCode();
        try {
            openPort(dispatch, Connection.CASH_MACHINE_COM_PORT);
            ResultData.getLastOperationResult(dispatch);

            result = getCashBoxSum(dispatch);
            ResultData.getLastOperationResult(dispatch);

        } catch (Exception e) {
            log.error("*** ReturnBail operation FAIL. Error: [" + e.getMessage() + "]");
        } finally {
            closePort(dispatch); // закрываем порт
        }
        return result;
    }

    @Override
    public ResultData printZReport() {
        log.info("Print Z report START.");
        ResultData resultData = ResultData.UNKNOWN;
        Dispatch dispatch = loadLib(Connection.PROGRAM_ID);
        try {
            resultData = openPort(dispatch, Connection.CASH_MACHINE_COM_PORT);

            resultData = printZReport(dispatch);

            log.info("Print Z report END. Operation status: " + getErrorInfo(dispatch));
        } catch (Exception e) {
            log.error(e.getMessage());
        } finally {
            closePort(dispatch);
        }
        return resultData;

    }

    @Override
    public ResultData printXReport() {
        log.info("Print X report START.");
        ResultData resultData = ResultData.UNKNOWN;
        Dispatch dispatch = loadLib(Connection.PROGRAM_ID);
        try {
            resultData = openPort(dispatch, Connection.CASH_MACHINE_COM_PORT);

            resultData = printXReport(dispatch);

            log.info("Print X report END. Operation status: " + getErrorInfo(dispatch));
        } catch (Exception e) {
            log.error(e.getMessage());
        } finally {
            closePort(dispatch);
        }
        return resultData;
    }

    @Override
    public String getCashMachineSN() {
        log.info("Get CashMachineSN START.");
        Dispatch dispatch = loadLib(Connection.PROGRAM_ID);
        String result = "";
        try {
            openPort(dispatch, Connection.CASH_MACHINE_COM_PORT);
            result = getCashMachineSN(dispatch);
            log.info("Get CashMachineSN END. Operation status: " + getErrorInfo(dispatch));
        } catch (Exception e) {
            log.error(e.getMessage());
        } finally {
            closePort(dispatch);
        }
        return result;
    }

    @Override
    public String getCashierId() {
        log.info("Get Cashier ID START.");
        Dispatch dispatch = loadLib(Connection.PROGRAM_ID);
        String result = "";
        try {
            openPort(dispatch, Connection.CASH_MACHINE_COM_PORT);
            result = String.valueOf(StatusUtils.getCashierId(dispatch));
            log.info("Get CashMachineSN END. Operation status: " + getErrorInfo(dispatch));
        } catch (Exception e) {
            log.error(e.getMessage());
        } finally {
            closePort(dispatch);
        }
        return result;
    }

    @Override
    public String getDateTimeShiftStart() {
        log.info("Get DateTime start fo current shift START.");
        Dispatch dispatch = loadLib(Connection.PROGRAM_ID);
        String result = "";
        try {
            openPort(dispatch, Connection.CASH_MACHINE_COM_PORT);
            result = StatusUtils.getStartDateCurrentShift(dispatch) + " " + StatusUtils.getStartTimeCurrentShift(dispatch);
            log.info("Get DateTime start fo current shift END. Operation status: " + getErrorInfo(dispatch));
        } catch (Exception e) {
            log.error(e.getMessage());
        } finally {
            closePort(dispatch);
        }
        return result;
    }

    @Override
    public Status getStatus() {
        log.info("Get CashMachine STATUS.");
        Dispatch dispatch = loadLib(Connection.PROGRAM_ID);
        Status status = null;
        try {
            openPort(dispatch, Connection.CASH_MACHINE_COM_PORT);
            status = StatusUtils.getCashMachineStatus(dispatch);
        } catch (Exception e) {
            log.error(e.getMessage());
        } finally {
            closePort(dispatch);
        }
        return status;
    }

    @Override
    public String getLastError(){
        Dispatch dispatch = loadLib(Connection.PROGRAM_ID);
        return getErrorInfo(dispatch);
    }

    @Override
    public ResultData cashierRegistration(String cashierId, String cashierPassword) {
        log.info("Cashier register request.");
        ResultData resultData = ResultData.UNKNOWN;
        Dispatch dispatch = loadLib(Connection.PROGRAM_ID);
        try {
           resultData = openPort(dispatch, Connection.CASH_MACHINE_COM_PORT);

           resultData = cashierRegistration(dispatch, cashierId, cashierPassword);

        } catch (Exception e) {
            log.error("Cashier registration FAIL. Error: [" + e.getMessage() + "]");
        } finally {
            closePort(dispatch); // закрываем порт
        }
        return resultData;
    }

    @Override
    public ResultData cancelReceipt() {
        log.info("Cancel receipt request.");
        ResultData resultData = ResultData.UNKNOWN;
        Dispatch dispatch = loadLib(Connection.PROGRAM_ID);
        try {
            resultData = openPort(dispatch, Connection.CASH_MACHINE_COM_PORT);

            cancelReceipt(dispatch);
            resultData = ResultData.getLastOperationResult(dispatch);
        } catch (Exception e) {
            log.error("Cancel receipt FAIL. Error: [" + e.getMessage() + "]");
        } finally {
            closePort(dispatch); // закрываем порт
        }
        return resultData;
    }

    @Override
    public ResultData printEmptyReceipt() {
        log.info("Print empty receipt request.");
        ResultData resultData = ResultData.UNKNOWN;
        Dispatch dispatch = loadLib(Connection.PROGRAM_ID);
        try {
            resultData = openPort(dispatch, Connection.CASH_MACHINE_COM_PORT);

            printEmptyReceipt(dispatch);
            resultData = ResultData.getLastOperationResult(dispatch);
        } catch (Exception e) {
            log.error("Print empty receipt FAIL. Error: [" + e.getMessage() + "]");
        } finally {
            closePort(dispatch); // закрываем порт
        }
        return resultData;
    }

    /**
     * Подключаемся к библиотеке фискального регистратора
     * @param programId
     * @return
     */
    private Dispatch loadLib(String programId) {
        //========JACOB
        //Loading the library:
        log.info("Start load activeX component. ProgramId: [{}]", programId);
        ActiveXComponent comp = new ActiveXComponent(programId);
        log.info("Component created");
        Object erc_object = comp.getObject();
        log.info("The Library been loaded, and an activeX component been created");
        return (Dispatch) erc_object;

    }

    /**
     * Открываем соединение с фискальным регитратором
     * @param dispatch
     * @param port
     * @return
     */
    private ResultData openPort(Dispatch dispatch, int port) {
        Dispatch.call(dispatch, Connection.CASHMACHINE_NAME, CommandsUtils.OPEN_PORT + ";" + port + ";115200;");
        String resultCode = getLastResultInfo(dispatch);
        String[] resultCodeTrim = resultCode.trim().split("[;]+");
        ResultData resultData = ResultData.getResultCode(Integer.valueOf(resultCodeTrim[0]));
        log.info(">>>>> Port open Success. >>>>>");
        return resultData;
    }

    /**
     * Закрываем соединение с фискальным регистраторм
     * @param dispatch
     */
    private void closePort(Dispatch dispatch) {
        Dispatch.call(dispatch, Connection.CASHMACHINE_NAME, CommandsUtils.CLOSE_PORT + ";");
        log.info("<<<<< Port close Success. <<<<<");
    }

    /**
     * Внос/выдача денег
     * @param dispatch
     * @param inOutFlag
     * @param sum
     * @return
     */
    private ResultData inOutOperation(Dispatch dispatch, int inOutFlag, int sum, String comment) {
        String command;
        if (comment == null) {
            command = CommandsUtils.IN_OUT + ";" + CASH + ";0;0;" + inOutFlag + ";" + sum + ".00;;;";
        } else {
            command = CommandsUtils.IN_OUT + ";" + CASH + ";1;2;" + inOutFlag + ";" + sum + ".00;" + comment + ";;";
        }
        Dispatch.call(dispatch, Connection.CASHMACHINE_NAME, command);
        log.info("In_Out operation request. Description: [{}]. Result: [{}]", getErrorInfo(dispatch), getLastResultInfo(dispatch));
        return ResultData.getLastOperationResultNoException(dispatch);

    }

    /**
     * Печать пустого чека
     * @param dispatch
     */
    private void printEmptyReceipt(Dispatch dispatch) {
        log.info("Print empty receipt request.");
        Dispatch.call(dispatch, Connection.CASHMACHINE_NAME, CommandsUtils.PRINT_EMPTY_RECEIPT + ";");
        log.info("Print empty receipt request. Result: " + getErrorInfo(dispatch));
    }

    /**
     * Дата/время фискального регистратора
     * @param dispatch
     * @return
     */
    private String getDateTimeCashMachine(Dispatch dispatch) {
        return Dispatch.call(dispatch, Connection.CASHMACHINE_NAME, CommandsUtils.GET_DATE_TIME + ";").getString();
    }

    /**
     * Открытие чека
     * @param dispatch
     * @param receiptFlag 0/1 возврат кредита/выдача залога   (продажа/отмена продажи)
     */
    private ResultData openReceipt(Dispatch dispatch, int receiptFlag) {
        log.info("Open receipt request.");
        Dispatch.call(dispatch, Connection.CASHMACHINE_NAME, CommandsUtils.OPEN_RECEIPT + ";" + receiptFlag + ";");
        log.info("Open receipt request. Description: [{}]. Result: [{}]", getErrorInfo(dispatch), getLastResultInfo(dispatch));
        return ResultData.getLastOperationResultNoException(dispatch);
    }

    /**
     * Отмена чека
     * @param dispatch
     * @throws ReceiptException
     */
    private void cancelReceipt(Dispatch dispatch) throws ReceiptException {
        log.info("Cancel receipt request.");
        int receiptStatus = StatusUtils.getReceiptStatus(dispatch);

        switch (receiptStatus) {
            case StatusUtils.RECEIPT_CLOSE:
                log.info("Cancel receipt request. Result: {}", "[Чек закрыт, дополнительная операция не требуется]");
                break;
            case 2:
                log.warn("Cancel receipt request. Result: {}", "[Чек возможно отменить только вручную. Выключите и включите РРО с нажатием кнопки продвижения ленты.]");
                throw new ReceiptException("Cancel receipt ERROR. Code: Чек возможно отменить только вручную. Выключите и включите РРО с нажатием кнопки продвижения ленты.");
            case 1:
            case 3: {
                Dispatch.call(dispatch, Connection.CASHMACHINE_NAME, CommandsUtils.CANCEL_RECEIPT + ";");
                log.info("Cancel receipt request. Result: {}", getLastResultInfo(dispatch));
                break;
            }
        }
    }

    /**
     * Продажа товара
     * @param dispatch
     * @param sum      - сумма
     * @param pluCode  - код товара
     * @return
     */
    private ResultData salePlu(Dispatch dispatch, int sum, int pluCode) {
        log.info("Sale PLU request.");
        Dispatch.call(dispatch, Connection.CASHMACHINE_NAME, CommandsUtils.SALE_PLU + ";0;0;1;1.000;" + pluCode + ";" + sum + ";");
        log.info("Sale PLU request. Description: [{}]. Result: [{}]", getErrorInfo(dispatch), getLastResultInfo(dispatch));
        return ResultData.getLastOperationResultNoException(dispatch);
    }

    /**
     * Добавить в чек скидка/наценка на товар
     * @param dispatch
     * @param operationalFlag
     * @param sum
     */
    private ResultData addDiscountSurcharge(Dispatch dispatch, int operationalFlag, int sum) {
        log.info("Add discount/surcharge request.");
        Dispatch.call(dispatch, Connection.CASHMACHINE_NAME, CommandsUtils.DISCOUNT_SURCHARGE + ";0;0;" + operationalFlag + ";" + sum + ".00;");
        log.info("Add discount/surcharge request. Description: [{}]. Result: [{}]", getErrorInfo(dispatch), getLastResultInfo(dispatch));
        return ResultData.getLastOperationResultNoException(dispatch);
    }

    /**
     * Добавить в чек комментарий
     * @param dispatch
     * @param message
     */
    private ResultData addComment(Dispatch dispatch, String message) {
        log.info("Add comment request.");
        Dispatch.call(dispatch, Connection.CASHMACHINE_NAME, CommandsUtils.COMMENT + ";0;1;0;0;0;0;" + message + ";");
        log.info("Add comment request. Description: [{}]. Result: [{}]", getErrorInfo(dispatch), getLastResultInfo(dispatch));
        return ResultData.getLastOperationResultNoException(dispatch);
    }

    /**
     * Оплата
     * @param dispatch
     * @param sum      всегда равна сумме залога
     */
    private ResultData pay(Dispatch dispatch, int sum) {
        log.info("Pay request.");
        Dispatch.call(dispatch, Connection.CASHMACHINE_NAME, CommandsUtils.PAY + ";" + CASH + ";" + sum + ".00;");
        log.info("Pay request. Description: [{}]. Result: [{}]", getErrorInfo(dispatch), getLastResultInfo(dispatch));
        return ResultData.getLastOperationResultNoException(dispatch);
    }

    /**
     * Печать копии последнего чека
     * @param dispatch
     */
    private ResultData lastReceiptCopy(Dispatch dispatch) {
        log.info("Receipt copy request.");
        Dispatch.call(dispatch, Connection.CASHMACHINE_NAME, CommandsUtils.PRINT_RECEIPT_COPY + ";");
        log.info("Receipt copy request. Description: [{}]. Result: [{}]", getErrorInfo(dispatch), getLastResultInfo(dispatch));
        return ResultData.getLastOperationResultNoException(dispatch);
    }

    /**
     * Регистрация кассира
     *
     * @param dispatch
     * @param cashierId
     * @param cashierPassword
     * @return
     */
    private ResultData cashierRegistration(Dispatch dispatch, String cashierId, String cashierPassword) {
        Dispatch.call(dispatch, Connection.CASHMACHINE_NAME, CommandsUtils.CASHIER_REGISTRATION + ";" + cashierId + ";" + cashierPassword + ";");
        String resultCode = getLastResultInfo(dispatch);
        String[] resultCodeTrim = resultCode.trim().split("[;]+");
        return ResultData.getResultCode(Integer.valueOf(resultCodeTrim[0]));
    }

    /**
     * Печять Х отчёта
     *
     * @param dispatch
     * @return
     */
    private ResultData printXReport(Dispatch dispatch) {
        Dispatch.call(dispatch, Connection.CASHMACHINE_NAME, CommandsUtils.EXECUTE_X_REPORT + ";" + Connection.ADMIN_PASSWORD + ";");
        String resultCode = getLastResultInfo(dispatch);
        String[] resultCodeTrim = resultCode.trim().split("[;]+");
        return ResultData.getResultCode(Integer.valueOf(resultCodeTrim[0]));
    }

    /**
     * Печять Z отчёта
     *
     * @param dispatch
     * @return
     */
    private ResultData printZReport(Dispatch dispatch) {
        Dispatch.call(dispatch, Connection.CASHMACHINE_NAME, CommandsUtils.EXECUTE_Z_REPORT + ";" + Connection.ADMIN_PASSWORD + ";");
        String resultCode = getLastResultInfo(dispatch);
        String[] resultCodeTrim = resultCode.trim().split("[;]+");
        return ResultData.getResultCode(Integer.valueOf(resultCodeTrim[0]));
    }

    /**
     * Сумма в кассе (наличные)
     * @param dispatch
     * @return
     */
    private int getCashBoxSum(Dispatch dispatch){
        log.info("Get CashBox sum request");
        Dispatch.call(dispatch, Connection.CASHMACHINE_NAME, CommandsUtils.GET_CASHBOX_SUM + ";");
        String result = getLastResultInfo(dispatch);
        String[] resultTrim = result.trim().split("[;]");
        Double cashSumDouble = Double.parseDouble(resultTrim[1]);
        Integer cashSum = cashSumDouble.intValue();
        log.info("Get CashBox sum request. Result: {}", result);
        return cashSum;
    }

    /**
     * Return serial number (only digits!!!)
     * @param dispatch
     * @return
     */
    private String getCashMachineSN(Dispatch dispatch){
        log.info("Get CashMachine SN request");
        Dispatch.call(dispatch, Connection.CASHMACHINE_NAME, CommandsUtils.GET_SERIAL_NUM + ";");
        String result = getLastResultInfo(dispatch);
        String[] resultTrim = result.trim().split("[;]");
        String serialNum = resultTrim[3];
        log.info("Get CashMachine SN request. Result: {}", result);
        return serialNum.replaceAll("\\D+","");
    }

    //---------------------------------------------------------//

    /**
     * @param dispatch
     * @return результат выполнения операции Dispatch.call(param)
     */
    private String getLastResultInfo(Dispatch dispatch) {
        return Dispatch.call(dispatch, CommandsUtils.GET_LAST_RESULT).getString();
    }

    /**
     * @param dispatch
     * @return Done if no error after Dispatch.call(param)
     */
    private String getErrorInfo(Dispatch dispatch) {
        return Dispatch.call(dispatch, CommandsUtils.GET_ERROR_INFO).getString();
    }

    //----------------------------------------------------------//

}
