/*
 * Copyright (c) 2017. Eugene Krotov. e.krotov@hotmail.com
 */

package net.lombard.rsnetlombard.services;


import net.lombard.rsnetlombard.domain.Status;
import net.lombard.rsnetlombard.utils.ResultData;

/**
 * @author Eugene Krotov e.krotov@hotmail.com
 * @version 1.0
 */
public interface CashMachineService {
    ResultData cashierRegistration(String cashierId, String cashierPassword);
    ResultData cashInOut(int sum, int inOutFlag, String comment);
    ResultData addBail(String bailIdent, int sum);
    ResultData sellBail(String bailIdent, int sum);
    ResultData toShopBail(String bailIdent, Integer sum);
    ResultData deleteBail(String bailIdent, Integer sum);
    ResultData returnBail(String bailIdent, int sum, int percent, int fine, int bonus);

    ResultData cancelReceipt();

    ResultData printEmptyReceipt();
    int getCashBoxSum();

    ResultData printZReport();

    ResultData printXReport();

    Status getStatus();
    String getLastError();
    String getCashMachineSN();
    String getCashierId();
    String getDateTimeShiftStart();

}
