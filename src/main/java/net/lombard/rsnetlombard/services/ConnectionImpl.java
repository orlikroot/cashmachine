/*
 * Copyright (c) 2017. Eugene Krotov. e.krotov@hotmail.com
 */

package net.lombard.rsnetlombard.services;

import lombok.extern.slf4j.Slf4j;
import net.lombard.rsnetlombard.domain.CashMachineOperation;
import net.lombard.rsnetlombard.utils.ResultData;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;

/**
 * @author Eugene Krotov e.krotov@hotmail.com
 * @version 1.0
 */
@Slf4j
public class ConnectionImpl implements Runnable, Connection {

    private String cashMachineSN;
    private Socket socket;
    private OutputStream outputStream;
    private InputStream inputStream;
    private ObjectInputStream objectInputStream;
    private ObjectOutputStream objectOutputStream;
    private boolean isAlive;

    public ConnectionImpl() {
        log.info("SERVER_URL: [{}] | SERVER_PORT: [{}] | CASHMACHINE_NAME: [{}] | PROGRAM_ID: [{}]", SERVER_URL, SERVER_PORT, CASHMACHINE_NAME, PROGRAM_ID);
        isAlive = true;
        Thread thread = new Thread(this);
        thread.setPriority(Thread.MAX_PRIORITY);
        thread.start();
    }

    @Override
    public void run() {
        try {
            socket = new Socket(InetAddress.getByName(Connection.SERVER_URL), Connection.SERVER_PORT);
            log.info("Socket created. Port: [{}]. Thread: [{}]", socket.getPort(), Thread.currentThread().getName());
            inputStream = socket.getInputStream();
            outputStream = socket.getOutputStream();

            while (isAlive) {
                int amount = inputStream.available();
                if (amount != 0) {
                    objectInputStream = new ObjectInputStream(inputStream);

                    CashMachineOperation message = (CashMachineOperation) objectInputStream.readObject();
                    log.info("New message From SERVER! Type: [{}]", message.getType());

                    sendMessage(receiveMessage(message));
                } else {
                    Thread.sleep(200);
                }
            }
        } catch (InterruptedException e) {
            log.error("InterruptedException", e);
        } catch (IOException e) {
            log.error("Connection to server LOST", e);
        } catch (ClassNotFoundException e) {
            log.error("ClassNotFoundException. Message: [{}]", e.getMessage(), e);
        }
    }

    @Override
    public synchronized void sendMessage(CashMachineOperation operation) {
        try {
            objectOutputStream = new ObjectOutputStream(outputStream);
            objectOutputStream.writeObject(operation);
            log.info("Send message SUCCESS");
        } catch (IOException e) {
            log.error("Send message ERROR. [{}]", e.getMessage(), e);
        }
    }

    @Override
    public synchronized CashMachineOperation receiveMessage(CashMachineOperation operation){
        CashMachineService cashMachineService = new CashMachineServiceImpl();
        CashMachineService service = new CashMachineServiceImpl();
        switch (operation.getType()){
            case CashMachineOperation.TYPE_PING: {
                operation.setStatus(CashMachineOperation.STATUS_OK);
                log.info("Ping from server");
                break;
            }
            case CashMachineOperation.TYPE_REGISTRATION: {
               String serialNum = service.getCashMachineSN();
               if(!serialNum.isEmpty()){
                   operation.setDeviceId(serialNum);
                   operation.setStatus(CashMachineOperation.STATUS_OK);
                   this.cashMachineSN = serialNum;
               } else {
                   operation.setStatus(CashMachineOperation.STATUS_UNKNOWN);
               }
                log.info("Registration request from server");
               break;
            }
            case CashMachineOperation.TYPE_CASH_IN_OUT: {
                ResultData resultData = cashMachineService.cashInOut(Math.abs(operation.getSum()), operation.getSum() > 0 ? CashMachineServiceImpl.IN : CashMachineServiceImpl.OUT, null);
                operation.setStatus(resultData.getCode());
                log.info("Cash IN_OUT request from server");

                break;
            }
            case CashMachineOperation.TYPE_ADD_BAIL: {
                ResultData resultData = cashMachineService.addBail(operation.getBailIdent() + " [" + String.valueOf(operation.getBailId()) + "]", operation.getSum());
                operation.setStatus(resultData.getCode());

                log.info("Add BAIL request from server");
                break;
            }
            case CashMachineOperation.TYPE_RETURN_BAIL: {
                ResultData resultData = cashMachineService.returnBail(operation.getBailIdent() + " [" + String.valueOf(operation.getBailId()) + "]", operation.getSum(), operation.getPercent(), operation.getFine(), operation.getBonus());
                operation.setStatus(resultData.getCode());

                log.info("Return BAIL request from server");
                break;
            }
            case CashMachineOperation.TYPE_SELL_BAIL: {
                ResultData resultData = cashMachineService.sellBail(operation.getBailIdent() + " [" + String.valueOf(operation.getBailId()) + "]", operation.getSum());
                operation.setStatus(resultData.getCode());

                log.info("Sell BAIL request from server");
                break;
            }
            case CashMachineOperation.TYPE_TO_SHOP_BAIL: {
                ResultData resultData = cashMachineService.toShopBail(operation.getBailIdent() + " [" + String.valueOf(operation.getBailId()) + "]", operation.getSum());
                operation.setStatus(resultData.getCode());

                log.info("To Shop BAIL request from server");
                break;
            }
            case CashMachineOperation.TYPE_DELETE_BAIL: {
                ResultData resultData = cashMachineService.deleteBail(operation.getBailIdent() + " [CANCEL]", operation.getSum());
                operation.setStatus(resultData.getCode());

                log.info("Delete BAIL request from server");
                break;
            }
        }
        return operation;
    }

    @Override
    public void close() {
        try {
            if (this.socket != null) {
                this.socket.close();
            }
            this.isAlive = false;
            log.info("Current connection to server was closed");
        } catch (IOException e) {
            log.error("Error close connection", e);        }
    }

    @Override
    public String getCashMachineSN() {
        return this.cashMachineSN;
    }

    @Override
    public Connection reconnect(){
        close();
        isAlive = true;
        Connection connection = new ConnectionImpl();
        log.info("Create new connection to server (reconnect). Old connection was closed.");
        return connection;
    }
}
