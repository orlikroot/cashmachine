/*
 * Copyright (c) 2017. Eugene Krotov. e.krotov@hotmail.com
 */

package net.lombard.rsnetlombard.services;

import net.lombard.rsnetlombard.domain.CashMachineOperation;
import net.lombard.rsnetlombard.utils.SettingsUtils;

/**
 * @author Eugene Krotov e.krotov@hotmail.com
 * @version 1.0
 */
public interface Connection {

    int CASH_MACHINE_COM_PORT = Integer.parseInt(SettingsUtils.getSettings().get(SettingsUtils.PORT));
    String PROGRAM_ID = SettingsUtils.getSettings().get(SettingsUtils.PROGRAM_ID);
    String CASHMACHINE_NAME = SettingsUtils.getSettings().get(SettingsUtils.CASHMACHINE_NAME);
    String SERVER_URL = SettingsUtils.getSettings().get(SettingsUtils.SERVER_URL);
    Integer SERVER_PORT = Integer.parseInt(SettingsUtils.getSettings().get(SettingsUtils.SERVER_PORT));
    String ADMIN_PASSWORD = SettingsUtils.getSettings().get(SettingsUtils.ADMIN_PASSWORD);

    void sendMessage(CashMachineOperation operation);
    CashMachineOperation receiveMessage(CashMachineOperation operation);
    void close();

    String getCashMachineSN();
    Connection reconnect();
}
