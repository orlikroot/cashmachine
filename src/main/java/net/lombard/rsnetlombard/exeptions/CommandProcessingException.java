/*
 * Copyright (c) 2017. Eugene Krotov. e.krotov@hotmail.com
 */

package net.lombard.rsnetlombard.exeptions;

/**
 * @author Eugene Krotov e.krotov@hotmail.com
 * @version 1.0
 */
public class CommandProcessingException extends Exception {
    public CommandProcessingException(String message){
        super(message);
    }
}
