/*
 * Copyright (c) 2017. Eugene Krotov. e.krotov@hotmail.com
 */

package net.lombard.rsnetlombard.exeptions;

/**
 * @author Eugene Krotov e.krotov@hotmail.com
 * @version 1.0
 */
public class DllException extends Exception {
    public DllException(String message){
        super(message);
    }
}
