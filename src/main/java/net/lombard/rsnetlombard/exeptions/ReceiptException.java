/*
 * Copyright (c) 2017. Eugene Krotov. e.krotov@hotmail.com
 */

package net.lombard.rsnetlombard.exeptions;

/**
 * @author Eugene Krotov e.krotov@hotmail.com
 * @version 1.0
 */
public class ReceiptException extends Exception {
    public ReceiptException(String message){
        super(message);
    }
}
