/*
 * Copyright (c) 2017. Eugene Krotov. e.krotov@hotmail.com
 */

package net.lombard.rsnetlombard.domain;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Eugene Krotov e.krotov@hotmail.com
 * @version 1.0
 */
@Data
@Builder
public class CashMachineOperation implements Serializable {

    static final long serialVersionUID = 1L;

    public static final int STATUS_ERROR_NO_CONNECTION = 600;
    public static final int STATUS_ERROR_NO_CASHMACHINE_IN_PW = 601;
    public static final int STATUS_ERROR_CASHMACHINE_UNREGISTERED_IN_PW = 602;
    public static final int TYPE_PING = 0;
    public static final int TYPE_REGISTRATION = 1;
    public static final int TYPE_CASH_IN_OUT = 2;
    public static final int TYPE_ADD_BAIL = 4;
    public static final int TYPE_RETURN_BAIL = 5;
    public static final int TYPE_TO_SHOP_BAIL = 6;
    public static final int TYPE_SELL_BAIL = 7;
    public static final int TYPE_DELETE_BAIL = 99;
    public static final int[] ALL_BASE_TYPE = {TYPE_CASH_IN_OUT, TYPE_ADD_BAIL, TYPE_RETURN_BAIL, TYPE_TO_SHOP_BAIL, TYPE_SELL_BAIL, TYPE_DELETE_BAIL};
    public static final int STATUS_OK = 0;
    public static final int STATUS_SEND = 500;
    public static final int STATUS_UNKNOWN = -1;
    public static final int STATUS_DELAYED = 501;
    public static final boolean PROCESS_COMPLETED = true;
    public static final boolean PROCESS_UNCOMPLETED = false;

    private Integer id;
    private String deviceId;
    private Integer type;
    private Integer bailId;
    private String bailIdent;
    private Integer sum;
    private Integer percent;
    private Integer fine;
    private Integer bonus;
    private Integer status;
    private boolean isProcessed;
    private Date createdOn;
    private Date processedOn;
}
