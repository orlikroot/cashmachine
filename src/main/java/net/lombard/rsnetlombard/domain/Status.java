/*
 * Copyright (c) 2017. Eugene Krotov. e.krotov@hotmail.com
 */

package net.lombard.rsnetlombard.domain;

import lombok.Builder;
import lombok.Data;

/**
 * @author Eugene Krotov e.krotov@hotmail.com
 * @version 1.0
 */
@Data
@Builder
public class Status {

    public static final int SHIFT_CLOSED = 0;
    public static final int SHIFT_OPEN = 1;

    public static final int RECEIPT_CLOSED = 0;
    public static final int RECEIPT_OPEN_TO_SALE = 1;
    public static final int RECEIPT_OPEN_TO_PAY = 2;
    public static final int RECEIPT_OPEN_TO_REFOUND = 3;

    public static final int BLOCKED = 1;
    public static final int NON_BLOCKED = 0;

    public static final int PERSONALIZED_TRUE = 0;
    public static final int PERSONALIZED_FALSE = 1;

    /**
     * PAR0
     * Код ошибки
     */
    int errorCode;

    /**
     * PAR1
     * Номер зарегистрированного кассира
     */
    Integer cashierId;

    /**
     * PAR2
     * Смена закрыта/открыта (0/1)
     * true - открыта/ false - закрыта
     */
    boolean isShiftOpen;

    /**
     * PAR3
     * Состояние чека:
      0 – Чек закрыт
      1 – Чек открыт для продажи
      2 – Чек открыт только для оплаты
      3 – Чек открыт для возврата
     */
    Integer receiptStatus;

    /**
     * PAR4
     * Отчеты по отделам (Х2) обнулены/не обнулены (0/1)
     * Не определено при par1=0
     */
    Integer reportX2;

    /**
     * PAR5
     * Отчеты по товарам (Х3) обнулены/не обнулены (0/1)
     * Не определено при par1=0
     */
    Integer reportX3;

    /**
     * PAR6
     * Отчеты по кассирам (Х5) обнулены не обнулены (0/1)
     * Не определено при par1=0
     */
    Integer reportX5;

    /**
     * PAR7
     * Отчеты по времени (Х6) обнулены/не обнулены(0/1)
     * Не определено при par1=0
     */
    Integer reportX6;

    /**
     * PAR8
     * Контрольная лента отпечатана/не отпечатана (0/1)
     * Не определено при par1=0
     */
    Integer controlTape;

    /**
     * PAR9
     * Длительность текущей смены не превышает 23 часа/превышает 23 часа (0/1)
     * true - не превышает / false - превышает
     */
    boolean isCurrentShiftDoesNotExceed23Hour;

    /**
     * PAR10
     * Длительность текущей смены не превышает 24 часа/превышает 24 часа (0/1)
     * true - не превышает / false - превышает
     */
    boolean isCurrentShiftDoesNotExceed24Hour;

    /**
     * PAR11
     * Дата начала смены дд.мм.гггг
     */
    String dateStartCurrentShift;

    /**
     * PAR12
     * Время начала смены чч:мм:сс
     */
    String timeStartCurrentShift;

    /**
     * PAR13
     * Сквозной номер смены (с начала эксплуатации ЭККА)
     */
    Integer throughShiftNumber;

    /**
     * PAR14
     * Номер текущей смены (для фискального режима, для нефискального режима всегда 0)
     */
    Integer currentShiftNumber;

    /**
     * PAR15
     * Номер последнего закрытого чека в текущей смене
     */
    Integer lastNumberReceiptClosedInCurrentShift;

    /**
     * PAR16
     * Номер последнего закрытого чека в предыдущей смене
     */
    Integer lastNumberReceiptClosedInPreviousShift;

    /**
     * PAR17
     * Количество использованных записей в базе товаров
     * Не определено при par1=0
     */
    Integer numberOfRecordsUsedInProductDatabase;

    /**
     * PAR18
     * Максимальное количество записей в базе товаров
     * Не определено при par1=0
     */
    Integer maxNumberOfProductInDatabase;

    /**
     * PAR19
     * Количество разрешенных кассиров
     */
    Integer numberOfAuthorizedCashiers;

    /**
     * PAR20
     * Номер записи
     */
    Integer numberOfRecord;

    /**
     * PAR21
     * Блокировка по причине не передачи отчетов в течении 72 часов
     * 0-не заблокирован, 1-заблокирован
     * false-не заблокирован, true-заблокирован
     */
    boolean isCashMachineBlockByReport;

    /**
     * PAR22
     * Блокировка по причине отсутствия персонализации
     * 0-не заблокирован, 1-заблокирован
     * false-не заблокирован, true-заблокирован
     */
    boolean isCashMachineBlockByPersonalization;

    /**
     * PAR23
     * Точка отсчета 72 часов до блокировки по причине непередачи отчетов , дата дд.мм.гггг
     * Если все данные переданы (дата/время блокировки не установлено), параметр будет 01.01.2000
     */
    String dateBeforeBlocking72HourNoReport;

    /**
     * PAR24
     * Точка отсчета 72 часов до блокировки по причине непередачи отчетов , время чч:мм:сс
     * Если все данные переданы (дата/время блокировки не установлено), параметр будет 00:00:00
     */
    String timeBeforeBlocking72HourNoReport;

    /**
     * PAR25
     * Количество переданных пакетов
     */
    Integer numberOfTransmittedPackets;

    /**
     * PAR26
     * Общее количество пакетов в памяти ЭККР
     */
    Integer numberOfPacketsInMemoryCashMachine;

    /**
     * PAR27
     * Состояние персонализации
     * 0 – Персонализован; 1 - не персонализован
     * true – Персонализован; false - не персонализован
     */
    boolean isPersonalized;

    /**
     * PAR28
     * ID_SAM
     */
    String ID_SAM;

    /**
     * PAR29
     * ID_DEV
     */
    String ID_DEV;

    @Override
    public String toString(){
        String report = new StringBuilder()
//                .append("Ошибка: " + ResultData.getResultCode(this.errorCode).getDescription() + "\n")
                .append("Кассир: " + this.cashierId + " (0 - не зарегестрирован)" +  "\n")
                .append("Смена: " + (this.isShiftOpen ? "открыта" : "закрыта") + "\n")
                .append("Дата начала смены: " + this.dateStartCurrentShift + "\n")
                .append("Время начала смены: " + this.timeStartCurrentShift + "\n")
                .append("ID ЭККА: " + this.ID_DEV + "\n")
                .toString();

        if(isCashMachineBlockByReport) {
            report += "Устройство заблокировано: Блокировка  \n по причине не передачи отчетов в течении 72 часов";
        }
        if(isCashMachineBlockByPersonalization) {
            report += "Устройство заблокировано: Блокировка  \n по причине отсутствия персонализации";
        }
        return report;
    }
}
