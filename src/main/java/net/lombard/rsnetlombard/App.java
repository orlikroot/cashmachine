/*
 * Copyright (c) 2017. Eugene Krotov. e.krotov@hotmail.com
 */

package net.lombard.rsnetlombard;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Created by Eugene Krotov on 5/23/2017.
 * @author Eugene Krotov e.krotov@hotmail.com
 * @version 1.0
 */
@Slf4j
public class App extends Application {

    private static final String FXML_FILE = "FXMLMain.fxml";
    public static String version;


    static {
        Properties prop = new Properties();
        try {
            //load a properties file from class path, inside static method
            prop.load(App.class.getClassLoader().getResourceAsStream("application.properties"));
            //get the property value
            version = prop.getProperty("version");
        } catch (IOException ex) {
            log.error("Error read version from application.properties", ex);
        }
    }

    private static final String TITLE = "ONIX Фискальный регистратор. ver. " + version;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {

        InputStream in = App.class.getClassLoader().getResourceAsStream(FXML_FILE);

        FXMLLoader loader = new FXMLLoader();
        AnchorPane root = (AnchorPane) loader.load(in);
        Scene scene = new Scene(root);
        primaryStage.setScene(scene);
        primaryStage.setTitle(TITLE);
        primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent t) {
                Platform.exit();
                System.exit(0);
            }
        });
        primaryStage.show();
    }
}
