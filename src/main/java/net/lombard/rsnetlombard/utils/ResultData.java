/*
 * Copyright (c) 2017. Eugene Krotov. e.krotov@hotmail.com
 */

package net.lombard.rsnetlombard.utils;


import com.jacob.com.Dispatch;
import lombok.extern.slf4j.Slf4j;
import net.lombard.rsnetlombard.exeptions.CommandProcessingException;
import net.lombard.rsnetlombard.exeptions.CommunicationChannelException;
import net.lombard.rsnetlombard.exeptions.DllException;
import net.lombard.rsnetlombard.services.Connection;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Eugene Krotov e.krotov@hotmail.com
 * @version 1.0
 */
@Slf4j
public enum ResultData {

    //Коды фискального регистратора
    COMMUNICATION_CHANNEL_ERROR_1 (1, "нет возможности запустить команду"),
    COMMUNICATION_CHANNEL_ERROR_2 (2, "невозможно обработать команду"),
    COMMUNICATION_CHANNEL_ERROR_3 (3, "код команды отсутствует"),
    COMMUNICATION_CHANNEL_ERROR_4 (4, "много данных в команде"),
    COMMUNICATION_CHANNEL_ERROR_5 (5, "недостаточно данных в команде"),
    COMMUNICATION_CHANNEL_ERROR_6 (6, "ошибка при приеме данных"),
    COMMUNICATION_CHANNEL_ERROR_7 (7, "недопустимый идентификатор команды"),
    COMMUNICATION_CHANNEL_ERROR_8 (8, "невозможно выполнить команду"),
    COMMUNICATION_CHANNEL_ERROR_10 (10, "Ошибка ввода"),

    COMMAND_PROCESSING_ERROR_11 (11, "необходимо снять z1 отчет"),
    COMMAND_PROCESSING_ERROR_12 (12, "скидки/наценки запрещены"),
    COMMAND_PROCESSING_ERROR_13 (13, "переполнение по чеку"),
    COMMAND_PROCESSING_ERROR_14 (14, "команда запрещена"),
    COMMAND_PROCESSING_ERROR_15 (15, "кассир не зарегистрирован"),
    COMMAND_PROCESSING_ERROR_16 (16, "отрицательная сумма"),
    COMMAND_PROCESSING_ERROR_17 (17, "количество товара отрицательное"),
    COMMAND_PROCESSING_ERROR_18 (18, "время смены исчерпано"),
    COMMAND_PROCESSING_ERROR_19 (19, "неверный тип оплаты"),
    COMMAND_PROCESSING_ERROR_20 (20, "неправильная или отсутствующая цена"),
    COMMAND_PROCESSING_ERROR_21 (21, "неверный параметр на входе команды"),
    COMMAND_PROCESSING_ERROR_22 (22, "товар находится в открытом чеке, нельзя редактировать"),
    COMMAND_PROCESSING_ERROR_23 (23, "некорректно запрограммированный товар"),
    COMMAND_PROCESSING_ERROR_24 (24, "неверный или отсутствующий штрих-код товара"),
    COMMAND_PROCESSING_ERROR_27 (27, "неверный или отсутствующий код товара"),
    COMMAND_PROCESSING_ERROR_28 (28, "товар не весовой(штучный)"),
    COMMAND_PROCESSING_ERROR_29 (29, "ФП почти заполнена"),
    COMMAND_PROCESSING_ERROR_30 (30, "ФП заполнена"),
    COMMAND_PROCESSING_ERROR_31 (31, "память инициализаций заполнена"),
    COMMAND_PROCESSING_ERROR_32 (32, "есть отложенная операция, оплата запрещена"),
    COMMAND_PROCESSING_ERROR_33 (33, "карточка клиента не принята"),
//    COMMAND_PROCESSING_ERROR_34 (34, "не хватает денег на сдачу"),
    COMMAND_PROCESSING_ERROR_34 (34, "в кассе не достаточно денег"),
    COMMAND_PROCESSING_ERROR_35 (35, "запрещена комбинированная оплата"),
    COMMAND_PROCESSING_ERROR_36 (36, "неправильный номер кассира"),
    COMMAND_PROCESSING_ERROR_37 (37, "места недостаточно"),
    COMMAND_PROCESSING_ERROR_38 (38, "нет места в Журнале"),
    COMMAND_PROCESSING_ERROR_39 (39, "нет места в базе товаров"),
    COMMAND_PROCESSING_ERROR_40 (40, "нет места в Архиве"),
    COMMAND_PROCESSING_ERROR_41 (41, "запрещено,  товар является  комплексом"),
    COMMAND_PROCESSING_ERROR_42 (42, "код не принадлежит комплексу"),
    COMMAND_PROCESSING_ERROR_43 (43, "ЭККА занят и не может выполнить команду"),
    COMMAND_PROCESSING_ERROR_44 (44, "необходимо снять 501 отчет"),
    COMMAND_PROCESSING_ERROR_45 (45, "неправильный пароль кассира"),
    COMMAND_PROCESSING_ERROR_46 (46, "комплекс невозможно продать"),
    COMMAND_PROCESSING_ERROR_47 (47, "цена товара определена"),
    COMMAND_PROCESSING_ERROR_48 (48, "отмена запрещена"),
    COMMAND_PROCESSING_ERROR_49 (49, "продажа товара запрещена"),
    COMMAND_PROCESSING_ERROR_50 (50, "ошибка чтения ФП"),
    COMMAND_PROCESSING_ERROR_51 (51, "номер производителя неверен"),
    COMMAND_PROCESSING_ERROR_52 (52, "ошибка записи во флеш"),
    COMMAND_PROCESSING_ERROR_54 (54, "удаление товара запрещено"),
    COMMAND_PROCESSING_ERROR_55 (55, "нет данных в фискальной памяти"),
    COMMAND_PROCESSING_ERROR_56 (56, "неправильный пароль налогового инспектора"),
    COMMAND_PROCESSING_ERROR_57 (57, "не готов выполнить команду"),
    COMMAND_PROCESSING_ERROR_58 (58, "неправильный пароль администратора"),
    COMMAND_PROCESSING_ERROR_60 (60, "РРО заблокирован (72 часа не было передачи данных)"),
    COMMAND_PROCESSING_ERROR_61 (61, "РРО не персонализован"),
    COMMAND_PROCESSING_ERROR_70 (70, "отсутсвует вал термоголовки"),
    COMMAND_PROCESSING_ERROR_79 (79, "дата сервисного обслуживания превышена"),
    COMMAND_PROCESSING_ERROR_80 (80, "ошибка записи в ФП"),
    COMMAND_PROCESSING_ERROR_81 (81, "ошибка часов"),
    COMMAND_PROCESSING_ERROR_82 (82, "ошибка данных в интерфейсе"),
    COMMAND_PROCESSING_ERROR_86 (86, "отсутствует индикатор клиента"),
    COMMAND_PROCESSING_ERROR_97 (97, "отсутствует бумага"),

    DLL_ERROR_300 (300, "неверная команда"),
    DLL_ERROR_301 (301, "ошибка порта"),
    DLL_ERROR_302 (302, "неверные параметры"),
    DLL_ERROR_303 (303, "неверное количество параметров"),
    DLL_ERROR_304 (304, "ошибка данных"),
    DLL_ERROR_305 (305, "порт закрыт"),
    DLL_ERROR_306 (306, "нет данных"),
    DLL_ERROR_307 (307, "не поддерживается"),
    DLL_ERROR_308 (308, "касса не доступна"),
    DLL_ERROR_309 (309, "неизвестный код ошибки"),
    DLL_ERROR_310 (310, "запись запрещена"),
    DLL_ERROR_311 (311, "ошибка чтения"),
    DLL_ERROR_312 (312, "ошибка записи"),
    DLL_ERROR_313 (313, "неправильная запись НДС"),
    DLL_ERROR_314 (314, "операция отменена"),
    DLL_ERROR_315 (315, "неправильная модель кассового аппарата"),
    DLL_ERROR_316 (316, "данные не существуют"),
    DLL_ERROR_317 (317, "кассовый аппарат отключен"),
    DLL_ERROR_318 (318, "файл не найден"),
    DLL_ERROR_319 (319, "присутствует ошибка"),
    DLL_ERROR_320 (320, "неверные данные"),
    DLL_ERROR_321 (321, "команда отключена"),
    DLL_ERROR_322 (322, "будет доступно в следующих версиях"),
    DLL_ERROR_323 (323, "принтер не готов"),

    //Коды операций (PERSONAL ERROR CODE)
    DONE (0, "Ок"),
    UNKNOWN (-1, "Неизвестный код"),
    RECEIPT_ERROR_501 (501, "чек открыт для другой операции");

    static Map<Integer, ResultData> errorsMap;
    static Map<Integer, ResultData> communicationErrorsMap;
    static Map<Integer, ResultData> commandErrorsMap;
    static Map<Integer, ResultData> dllErrorsMap;
    static int [] communicationErrors = {1,2,3,4,5,6,7,8,10};
    static int [] commandErrors = {11,12,13,14,15,16,17,18,19,
            20,21,22,23,24,27,28,29,
            30,31,32,33,34,35,36,37,38,39,
            40,41,42,43,44,45,46,47,48,49,
            50,51,52,54,55,56,57,58,
            60,61,
            70,79,
            80,81,82,86,
            97};
    static int [] dllErrors = {301,302,303,304,305,306,307,308,309,
            310, 311,312,313,314,315,316,317,318,319,320,
            321,322,323};

    static {
        errorsMap = new HashMap<>();
        communicationErrorsMap = new HashMap<>();
        commandErrorsMap = new HashMap<>();
        dllErrorsMap = new HashMap<>();
        for(ResultData resultData : ResultData.values()){
            errorsMap.put(resultData.getCode(), resultData);
        }
        for(int code : communicationErrors){
            communicationErrorsMap.put(code, errorsMap.get(code));
        }
        for(int code : commandErrors){
            commandErrorsMap.put(code, errorsMap.get(code));
        }
        for(int code : dllErrors){
            dllErrorsMap.put(code, errorsMap.get(code));
        }
    }

    private final int code;
    private final String description;

    ResultData(int code, String description) {
        this.code = code;
        this.description = description;
    }

    public static ResultData getResultCode(Integer code){
        if(errorsMap.containsKey(code)){
            return  errorsMap.get(code);
        } else
            return ResultData.UNKNOWN;
    }

    public static ResultData getLastOperationResult(Dispatch dispatch) throws CommunicationChannelException, CommandProcessingException, DllException {
        String resultCode = Dispatch.call(dispatch, CommandsUtils.GET_LAST_RESULT).getString();
        String[] sTrim = resultCode.trim().split("[;]+");
        ResultData resultData = getResultCode(Integer.valueOf(sTrim[0]));
        Dispatch.call(dispatch, Connection.CASHMACHINE_NAME, CommandsUtils.LAST_EVENT + ";");
        String lastEvent = Dispatch.call(dispatch, CommandsUtils.GET_LAST_RESULT).getString();
        if(resultData.getCode() != 0) {
            log.error("*** [{}] operation ERROR. Code: {} | Description: {}", lastEvent, resultData.code, resultData.description);
        }


        if(communicationErrorsMap.containsKey(resultData.code)){
            throw new CommunicationChannelException(resultData.description);
        }
        if(commandErrorsMap.containsKey(resultData.code)){
            throw new CommandProcessingException(resultData.description);
        }
        if(dllErrorsMap.containsKey(resultData.code)){
            throw new DllException(resultData.description);
        }

        return resultData;
    }

    public static ResultData getLastOperationResultNoException(Dispatch dispatch){
        String resultCode = Dispatch.call(dispatch, CommandsUtils.GET_LAST_RESULT).getString();
        String[] sTrim = resultCode.trim().split("[;]");
        ResultData resultData = getResultCode(Integer.valueOf(sTrim[0]));
        Dispatch.call(dispatch, Connection.CASHMACHINE_NAME, CommandsUtils.LAST_EVENT + ";");
        String lastEvent = Dispatch.call(dispatch, CommandsUtils.GET_LAST_RESULT).getString();
        if(resultData.getCode() != 0) {
            log.error("*** [{}] operation ERROR. Code: {} | Description: {}", lastEvent, resultData.code, resultData.description);
        }
        return resultData;
    }

    public String getDescription() {
        return description;
    }

    public int getCode() {
        return code;
    }

    @Override
    public String toString() {
        return code + ": " + description;
    }
}
