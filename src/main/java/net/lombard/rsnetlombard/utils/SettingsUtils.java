/*
 * Copyright (c) 2017. Eugene Krotov. e.krotov@hotmail.com
 */

package net.lombard.rsnetlombard.utils;

import lombok.extern.slf4j.Slf4j;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Eugene Krotov e.krotov@hotmail.com
 * @version 1.0
 */
@Slf4j
public class SettingsUtils {

    public static final String PORT = "port";
    public static final String PROGRAM_ID = "programId";
    public static final String CASHMACHINE_NAME = "cashMachineName";
    public static final String SERVER_URL = "serverUrl";
    public static final String SERVER_PORT = "serverPort";
    public static final String ADMIN_PASSWORD = "adminPassword";


    private SettingsUtils() {
        throw new IllegalStateException("Utility class");
    }

    public static Map<String, String> getSettings() {
        Map<String, String> settings = new HashMap<>();
        try {

            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser saxParser = factory.newSAXParser();

            DefaultHandler handler = new DefaultHandler() {

                boolean port = false;
                boolean programId = false;
                boolean cashMachineName = false;
                boolean serverUrl = false;
                boolean serverPort = false;
                boolean adminPassword = false;

                @Override
                public void startElement(String uri, String localName, String qName,
                                         Attributes attributes) throws SAXException {

                    if (qName.equalsIgnoreCase(PORT)) {
                        port = true;
                    }

                    if (qName.equalsIgnoreCase(PROGRAM_ID)) {
                        programId = true;
                    }

                    if (qName.equalsIgnoreCase(CASHMACHINE_NAME)) {
                        cashMachineName = true;
                    }

                    if (qName.equalsIgnoreCase(SERVER_URL)) {
                        serverUrl = true;
                    }

                    if (qName.equalsIgnoreCase(SERVER_PORT)) {
                        serverPort = true;
                    }

                    if (qName.equalsIgnoreCase(ADMIN_PASSWORD)) {
                        adminPassword = true;
                    }
                }

                @Override
                public void endElement(String uri, String localName,
                                       String qName) throws SAXException {
                }

                @Override
                public void characters(char ch[], int start, int length) throws SAXException {

                    if (port) {
                        settings.put(PORT, new String(ch, start, length));
                        port = false;
                    }

                    if (programId) {
                        settings.put(PROGRAM_ID, new String(ch, start, length));
                        programId = false;
                    }

                    if (cashMachineName) {
                        settings.put(CASHMACHINE_NAME, new String(ch, start, length));
                        cashMachineName = false;
                    }

                    if (serverUrl) {
                        settings.put(SERVER_URL, new String(ch, start, length));
                        serverUrl = false;
                    }

                    if (serverPort) {
                        settings.put(SERVER_PORT, new String(ch, start, length));
                        serverPort = false;
                    }
                    if (adminPassword) {
                        settings.put(ADMIN_PASSWORD, new String(ch, start, length));
                        adminPassword = false;
                    }
                }
            };

            File currentDirFile = new File("settings.xml");
            String helper = currentDirFile.getAbsolutePath();
            log.info("Setting.xml file path: [{}]", helper);
            saxParser.parse(helper, handler);

        } catch (Exception e) {
            log.error("Error read settings.xml. Message: [{}]", e.getMessage(), e);
        }
        return settings;
    }
}
