/*
 * Copyright (c) 2017. Eugene Krotov. e.krotov@hotmail.com
 */

package net.lombard.rsnetlombard.utils;

/**
 * @author Eugene Krotov e.krotov@hotmail.com
 * @version 1.0
 */
public class CommandsUtils {

    public static final String OPEN_PORT = "open_port";
    public static final String CLOSE_PORT = "close_port";
    public static final String SHOW_ERROR_FLAGS = "show_error_flags";
    public static final String GET_SOFT_VERSION = "get_soft_version";
    public static final String GET_KSEF_STATUS = "get_ksef_status";
    public static final String GET_KSEF = "get_KSEF";
    public static final String GET_STATUS = "get_status";
    public static final String GET_DATE_TIME = "get_date_time";
    public static final String SET_DATE = "set_date";
    public static final String SET_TIME = "set_time";
    public static final String INDICATE = "indicate";
    public static final String SET_HEADER = "set_header";
    public static final String GET_HEADER = "get_header";
    public static final String SET_CASHIERS_NUMBER = "set_cashiers_number";
    public static final String SET_TAXES = "set_taxes";
    public static final String GET_TAXES = "get_taxes";
    public static final String CASHIER_REGISTRATION = "cashier_registration";
    public static final String ADD_PLU = "add_plu";
    public static final String CHANGE_QTY = "change_qty";
    public static final String DEL_PLU = "del_plu";
    public static final String GET_PLU_INFO = "get_plu_info";
    public static final String ADD_RENEW_COMPLEX = "add_renew_complex";
    public static final String DEL_COMPLEX = "del_complex";
    public static final String GET_COMPLEX_INFO = "get_complex_info";
    public static final String DECODE_COMPLEX_TO_FILE = "decode_complex_to_file";
    public static final String IN_OUT = "in_out";
    public static final String OPEN_RECEIPT = "open_receipt";
    public static final String SALE_PLU = "sale_plu";
    public static final String DISCOUNT_SURCHARGE = "discount_surcharge";
    public static final String PAY = "pay";
    public static final String PROG_PENDING_OPERATION = "progr_pending_operation";
    public static final String PERFORM_PENDING_OPERATION = "perform_pending_operation";
    public static final String VIEW_PENDING_OPERATION = "view_pending_operation";
    public static final String CANCEL_RECEIPT = "cancel_receipt";
    public static final String PRINT_EMPTY_RECEIPT = "print_empty_receipt";
    public static final String PRINT_RECEIPT_COPY = "print_receipt_copy";
    public static final String COMMENT = "comment";
    public static final String PRINT_QRCODE = "print_qrcode";
    public static final String GET_LAST_RECEIPT_NUMBER = "get_last_receipt_number";
    public static final String GET_CASHBOX_SUM = "get_cashbox_sum";
    public static final String PAPER_FEED = "paper_feed";
    public static final String SET_DIR = "set_dir";
    public static final String GET_DIR = "get_dir";
    public static final String WRITE_TABLE = "write_table";
    public static final String READ_TABLE = "read_table";
    public static final String GET_FM_STATUS = "get_FM_status";
    public static final String READ_FM_TABLE = "read_fm_table";
    public static final String GET_FLASH = "get_flash";
    public static final String GET_PLUS = "get_PLUs";
    public static final String GET_EJ = "get_EJ";
    public static final String GET_REPORT = "get_report";
    public static final String EXECUTE_X_REPORT = "execute_X_report";
    public static final String EXECUTE_Z_REPORT = "execute_Z_report";
    public static final String EXECUTE_REPORT = "execute_report";
    public static final String CLEAR_REPORT = "clear_report";
    public static final String ZERO_ALL_REPORTS = "zero_all_reports";
    public static final String GET_LAST_ERROR = "get_last_error";
    public static final String GET_LIB_VERSION = "get_lib_version";
    public static final String GET_SERIAL_NUM = "get_serial_num";
    public static final String GET_SYS_MONITOR = "get_sys_monitor";
    public static final String GET_DEV_ID = "get_dev_id";
    public static final String KBLOCK = "kblock";
    public static final String LAST_EVENT = "last_event";
    public static final String PRINT_CHAR_TABLE = "printer_char_table";
    public static final String RESET_ECR_ERROR = "reset_ecr_error";
    public static final String GETEXT_Z1REP_INFO = "getext_z1rep_info";
    public static final String PUT_LOGO = "put_logo";
    public static final String INFO_LOGO = "info_logo";
    public static final String ACTIVATE_LOGO = "activate_logo";
    public static final String DEACTIVATE_LOGO = "deactivate_logo";
    public static final String SET_CONTRAST = "set_contrast";
    public static final String CUT_PAPER = "cut_paper";
    public static final String EXECUTE_FILE = "execute_file";
    public static final String SEND_CMD = "send_cmd";
    public static final String OPEN_CASHBOX = "open_cashbox";
    public static final String CANSEL_COMMAND = "сancel_command";
    public static final String PRINT_SETTINGS = "print_settings";
    public static final String SHOW_SUBTOTAL = "show_subtotal";
    public static final String SET_ERROR_LOG = "set_error_log";
    public static final String PERSONALIZE = "personalize";
    public static final String DPS = "dps";
    public static final String SET_INDICATOR = "set_indicator";
    public static final String PRINTERISREADY = "printerisready";
    public static final String SET_CUTTER = "set_cutter";
    public static final String SET_TIMEOUT = "set_timeout";

    public static final String GET_LAST_RESULT = "get_last_result";  // команда не документирована
    public static final String GET_ERROR_INFO = "get_error_info";  // команда не документирована

    private CommandsUtils() {
        throw new IllegalAccessError("Utility class");
    }
}
