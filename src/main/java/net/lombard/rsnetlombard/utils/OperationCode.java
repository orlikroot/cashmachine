/*
 * Copyright (c) 2017. Eugene Krotov. e.krotov@hotmail.com
 */

package net.lombard.rsnetlombard.utils;

/**
 * @author Eugene Krotov e.krotov@hotmail.com
 * @version 1.0
 */
public enum OperationCode {

    CASH_IN_OPERATION (1, "служебное пополнение"),
    CASH_OUT_OPERATION (2, "служебное снятие"),
    ADD_BAIL_OPERATION (3, "выдача залога"),
    RETURN_BAIL_OPERATION (4, "возврат залога");

    private final int code;
    private final String description;

    OperationCode(int code, String description) {
        this.code = code;
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public int getCode() {
        return code;
    }

    @Override
    public String toString() {
        return code + ": " + description;
    }
}
