/*
 * Copyright (c) 2017. Eugene Krotov. e.krotov@hotmail.com
 */

package net.lombard.rsnetlombard.utils;

import com.jacob.com.Dispatch;
import lombok.extern.slf4j.Slf4j;
import net.lombard.rsnetlombard.domain.Status;
import net.lombard.rsnetlombard.services.Connection;

/**
 * @author Eugene Krotov e.krotov@hotmail.com
 * @version 1.0
 */
@Slf4j
public class StatusUtils {

    public static final int RECEIPT_CLOSE = 0;

    private StatusUtils() {
        throw new IllegalStateException("Utility class");
    }

    /**
     * Проверка состояния чека
     * @param dispatch
     * @return
     */
    public static Integer getReceiptStatus(Dispatch dispatch){
        String status = getStatus(dispatch);
        String[] statusTrim = status.trim().split("[;]");
        Integer receiptStatusCode = Integer.valueOf(statusTrim[3]);
        switch (receiptStatusCode){
            case RECEIPT_CLOSE :
                log.info("Receipt status request. Response: {}", "[Чек закрыт]");
                break;
            case 1 :
                log.info("Receipt status request. Response: {}", "[Чек открыт для продажи]");
                break;
            case 2 :
                log.info("Receipt status request. Response: {}", "[Чек открыт только для оплаты]");
                break;
            case 3 :
                log.info("Receipt status request. Response: {}", "[Чек открыт для возврата]");
                break;
        }
        return receiptStatusCode;
    }

    /**
     *
     * @param dispatch
     * @return Id зарегетритованного кассира
     */
    public static Integer getCashierId(Dispatch dispatch){
        String status = getStatus(dispatch);
        String[] statusTrim = status.trim().split("[;]");
        Integer cashierId = Integer.valueOf(statusTrim[1]);
        log.info("Cashier ID request. Response: [{}]", cashierId);
        return cashierId;
    }

    /**
     * Проверяем, открыта смена или нет
     * @param dispatch
     * @return
     */
    public static boolean isShiftOpen(Dispatch dispatch){
        String status = getStatus(dispatch);
        String[] statusTrim = status.trim().split("[;]");
        Integer statusCode = Integer.valueOf(statusTrim[2]);
        boolean result = false;
        if(statusCode == 1){
            result = true;
        }
        log.info("Is shift open request. Response: [{}]", result);
        return result;
    }

    /**
     * Проверяем, текущая смена длится более 23 часов или нет
     * @param dispatch
     * @return
     */
    public static boolean isCurrentShitMoreThen23Hours(Dispatch dispatch){
        String status = getStatus(dispatch);
        String[] statusTrim = status.trim().split("[;]");
        Integer statusCode = Integer.valueOf(statusTrim[9]);
        boolean result = false;
        if(statusCode == 1){
            result = true;
        }
        log.info("Current shift more then 23 hours request. Response: [{}]", result);
        return result;
    }

    /**
     * Проверяем, текущая смена длится более 24 часов или нет
     * @param dispatch
     * @return
     */
    public static boolean isCurrentShitMoreThen24Hours(Dispatch dispatch){
        String status = getStatus(dispatch);
        String[] statusTrim = status.trim().split("[;]");
        Integer statusCode = Integer.valueOf(statusTrim[10]);
        boolean result = false;
        if(statusCode == 1){
            result = true;
        }
        log.info("Current shift more then 24 hours request. Response: [{}]", result);
        return result;
    }

    /**
     * Дата начала смены
     * @param dispatch
     * @return
     */
    public static String getStartDateCurrentShift(Dispatch dispatch){
        String status = getStatus(dispatch);
        String[] statusTrim = status.trim().split("[;]");
        String result = statusTrim[11];
        log.info("Current shift start Date request. Response: [{}]", result);
        return result;
    }

    /**
     * Время начала смены
     * @param dispatch
     * @return
     */
    public static String getStartTimeCurrentShift(Dispatch dispatch){
        String status = getStatus(dispatch);
        String[] statusTrim = status.trim().split("[;]");
        String result = statusTrim[12];
        log.info("Current shift start Date request. Response: [{}]", result);
        return result;
    }

    /**
     * Сквозной номер смены с начала работы фискального регистратора
     * @param dispatch
     * @return
     */
    public static int getShiftNumber(Dispatch dispatch){
        String status = getStatus(dispatch);
        String[] statusTrim = status.trim().split("[;]");
        Integer shift = Integer.valueOf(statusTrim[13]);
        log.info("Shift number request. Response: [{}]", shift);
        return shift;
    }

    /**
     * Номер последнего чека в текущей смене
     * @param dispatch
     * @return
     */
    public static int getLastReceiptNumberCurrentShift(Dispatch dispatch){
        String status = getStatus(dispatch);
        String[] statusTrim = status.trim().split("[;]");
        Integer receiptNumber = Integer.valueOf(statusTrim[15]);
        log.info("Last receipt number in the current shift request. Response: [{}]", receiptNumber);
        return receiptNumber;
    }

    /**
     * Номер последнего чека в предидущей смене
     * @param dispatch
     * @return
     */
    public static int getLastReceiptNumberPreviousShift(Dispatch dispatch){
        String status = getStatus(dispatch);
        String[] statusTrim = status.trim().split("[;]");
        Integer receiptNumber = Integer.valueOf(statusTrim[16]);
        log.info("Last receipt number in the previous shift request. Response: [{}]", receiptNumber);
        return receiptNumber;
    }

    /**
     * Блокировка по причине не передачи отчетов в течении 72 часов
     * @param dispatch
     * @return
     */
    public static boolean isCashMachineBlockByReport(Dispatch dispatch){
        String status = getStatus(dispatch);
        String[] statusTrim = status.trim().split("[;]");
        Integer result = Integer.valueOf(statusTrim[21]);
        log.info("Is CashMashine blocking due to non-transmission of reports within 72 hours request. Response: [{}]", result == 0 ? false : true);
        return result == 0 ? false : true;
    }

    /**
     * Блокировка по причине отсутствия персонализации
     * @param dispatch
     * @return
     */
    public static boolean isCashMachineBlockByPersonalization(Dispatch dispatch){
        String status = getStatus(dispatch);
        String[] statusTrim = status.trim().split("[;]");
        Integer result = Integer.valueOf(statusTrim[22]);
        log.info("Is CashMashine blocking due to non-transmission of reports within 72 hours request. Response: [{}]", result == 0 ? false : true);
        return result == 0 ? false : true;
    }

    public static Status getCashMachineStatus(Dispatch dispatch){
        String statusStr = getStatus(dispatch);
        String[] statusTrim = statusStr.trim().split("[;]");

        boolean isCashierDefine = Integer.parseInt(statusTrim[1]) == 0 ? false : true;

        Status status = Status.builder()
                .errorCode(Integer.parseInt(statusTrim[0]))
                .cashierId(Integer.parseInt(statusTrim[1]))
                .isShiftOpen(Integer.parseInt(statusTrim[2]) == 0 ? false : true)
                .receiptStatus(Integer.parseInt(statusTrim[3]))
                .reportX2(isCashierDefine ? Integer.parseInt(statusTrim[4]) : null)
                .reportX3(isCashierDefine ? Integer.parseInt(statusTrim[5]) : null)
                .reportX5(isCashierDefine ? Integer.parseInt(statusTrim[6]) : null)
                .reportX6(isCashierDefine ? Integer.parseInt(statusTrim[7]) : null)
                .controlTape(isCashierDefine ? Integer.parseInt(statusTrim[8]) : null)
                .isCurrentShiftDoesNotExceed23Hour(Integer.parseInt(statusTrim[9]) == 0 ? true : false)
                .isCurrentShiftDoesNotExceed24Hour(Integer.parseInt(statusTrim[10]) == 0 ? true : false)
                .dateStartCurrentShift(statusTrim[11])
                .timeStartCurrentShift(statusTrim[12])
                .throughShiftNumber(Integer.parseInt(statusTrim[13]))
                .currentShiftNumber(Integer.parseInt(statusTrim[14]))
                .lastNumberReceiptClosedInCurrentShift(Integer.parseInt(statusTrim[15]))
                .lastNumberReceiptClosedInPreviousShift(Integer.parseInt(statusTrim[16]))
                .numberOfRecordsUsedInProductDatabase(isCashierDefine ? Integer.parseInt(statusTrim[17]) : null)
                .maxNumberOfProductInDatabase(isCashierDefine ? Integer.parseInt(statusTrim[18]) : null)
                .numberOfAuthorizedCashiers(Integer.parseInt(statusTrim[19]))
                .numberOfRecord(Integer.parseInt(statusTrim[20]))
                .isCashMachineBlockByReport(Integer.parseInt(statusTrim[21]) == 0 ? false : true)
                .isCashMachineBlockByPersonalization(Integer.parseInt(statusTrim[22]) == 0 ? false : true)
                .dateBeforeBlocking72HourNoReport(statusTrim[23])
                .timeBeforeBlocking72HourNoReport(statusTrim[24])
                .numberOfTransmittedPackets(Integer.parseInt(statusTrim[25]))
                .numberOfPacketsInMemoryCashMachine(Integer.parseInt(statusTrim[26]))
                .isPersonalized(Integer.parseInt(statusTrim[27]) == 0 ? true : false)
                .ID_SAM(statusTrim[28])
                .ID_DEV(statusTrim[29])
                .build();

        return status;
    }

    /**
     * Показать системные ошибки
     * @param dispatch
     * @return
     */
    private static String getSysMonitor(Dispatch dispatch){
        Dispatch.call(dispatch, Connection.CASHMACHINE_NAME, CommandsUtils.GET_SYS_MONITOR +  ";");
        String result = Dispatch.call(dispatch, CommandsUtils.GET_LAST_RESULT).getString();
        log.info("System monitor request. Result: [{}]", result);
        return result;
    }

    /**
     * Статус фискального регистратора
     * @param dispatch
     * @return Первое значение - код ошибки, далее - параметры
     */
    private static String getStatus(Dispatch dispatch){
        Dispatch.call(dispatch, Connection.CASHMACHINE_NAME, CommandsUtils.GET_STATUS +  ";1;"); //проверяем состояние регистратора; 1/0 полный/не полный ответ
        String result = Dispatch.call(dispatch, CommandsUtils.GET_LAST_RESULT).getString();
        log.info("Status request. Result: [{}]", result);
        return result;
    }
}
